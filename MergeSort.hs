module MergeSort where

import System.Random (newStdGen, randomRs)

randomIntList :: Int -> Int -> Int -> IO [Int]
randomIntList n from to = take n . randomRs (from, to) <$> newStdGen

mergeSort :: [Int] -> [Int]
mergeSort [] = []
mergeSort (x:[]) = [x]
mergeSort list = let pair = getTwoHalfs list in
    merge (mergeSort (fst pair)) (mergeSort (snd pair))

merge :: [Int] -> [Int] -> [Int]
merge [] y = y
merge x [] = x
merge (x:xs) (y:ys) = if x < y then x:(merge xs (y: ys)) else y: (merge (x:xs) ys)

getTwoHalfs :: [Int] -> ([Int], [Int])
getTwoHalfs [] = ([], [])
getTwoHalfs (x:[]) = ([x], [])
getTwoHalfs (x:p:xs) = (x:fst pair, p:snd pair) where pair = getTwoHalfs xsS